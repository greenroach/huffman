#include "stdafx.h"
#include "Node.h"
#include <iostream>

using std::cout;
using std::endl;

Node* Node::root = nullptr;
std::vector<Node *> Node::lastNodes = std::vector<Node *>();;

Node::Node()
{
	prev = nullptr;
	next0 = nullptr;
	next1 = nullptr;
	code = 0;
	p = 0;
	value = 0;
}

Node::Node(const float p):Node()
{
	this->p = p;
}

Node::~Node()
{
	prev = nullptr;
	next0 = nullptr;
	next1 = nullptr;
	code = 0;
	p = 0;
	value = 0;
}

void Node::linkTo(Node* prev, Node* next0, Node* next1) {
	this->prev = prev;
	this->next0 = next0;
	this->next1 = next1;
}

void Node::linkTo(Node*next0, Node*next1) {
	this->next0 = next0;
	this->next1 = next1;
}

void Node::linkTo(Node*prev) {
	this->prev = prev;
}


