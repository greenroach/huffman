#pragma once

#include "Huffman.h"
#include <opencv2/core/core.hpp>
#include <functional>
#include <iostream>
#include <string>

using namespace cv;
using std::cout;
using std::cin;
using std::endl;

/* main */
Mat decode(string filename);
void encode(Mat& image, string filename);

/* zigzag scan */
void getZigzagCode(const std::function<void(int, int, int)> callback);
vector<int> matToVecZigZag(Mat& matrix);
Mat vecToMatZigZag(vector<int>& codes);

/* rle */
vector<int> rleEncode(vector<int> vec);
void rleDecode(vector<int>& codes, vector<int>& result);

/* utils */
template<typename T>
void printMat(Mat mat, string str);
void printMat(Mat mat, string str);
int calc_mat_size(const int size);
Mat read_examples(const string filename, const int rows, const int cols, const int type);
void print_loader();