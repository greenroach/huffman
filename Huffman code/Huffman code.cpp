// Huffman code.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <map>
#include "Header.h"
#include <fstream>
#include <opencv2/highgui.hpp>
#include <bitset>

const int SIZE = 8;
const int WORD = 8;
const int DWORD = 16;

int main() {
	Mat image = cv::imread("cat_ghost.bmp");
	Mat gray;
	
	cv::cvtColor(image, gray, CV_BGR2GRAY);
	cv::imshow("source", gray);
	waitKey(1);

	const auto file = "image.bin";
	encode(image, file);
	const auto result = decode(file);
	
	imshow("Result", result);
	waitKey(0);
	return 0;
}

/**
 * \brief ����������� �����������
 * \param image  ������� �����������
 * \param filename �������� ����
 */
void encode(Mat& image, string filename) {
	/*��������� ����������� � ������ YCrCb � ��������� ����� Y*/
	vector<Mat> planes;

	cv::cvtColor(image, image, CV_BGR2YCrCb);
	cv::split(image, planes);
	auto tempImage = planes[0];

	/*�������� �������� � ����������� �������*/
	const Mat extMat = Mat(calc_mat_size(image.rows), calc_mat_size(image.cols), CV_8U, Scalar::all(0));
	tempImage.copyTo(extMat(Rect(0, 0, tempImage.cols, tempImage.rows)));

	//��������� ������� ����������� 8x8
	const auto quantizationMat = read_examples("quantable8x8.txt", SIZE, SIZE, CV_32F);

	std::vector<Mat> matrixList = std::vector<Mat>();
	std::stringstream builder = std::stringstream();
	std::stringstream buff = std::stringstream();

	/*������ ������ ����������*/
	builder << std::bitset<WORD>(0xff);
	builder << std::bitset<WORD>(0x44);
	builder << std::bitset<DWORD>(extMat.rows);
	builder << std::bitset<DWORD>(extMat.cols);
	cout << endl << "coding...   ";

	for (int i = 0; i < extMat.cols; ++i) {
		for (int j = 0; j < extMat.rows; ++j) {
			
			print_loader();

			const auto modI = i % SIZE;
			const auto modJ = j % SIZE;

			if (!modI && !modJ) {
				Mat dctMat;
				/* ����� ����������� �� ������� �������� SIZE x SIZE */
				auto mat_fragment = Mat(extMat, Rect(i, j, SIZE, SIZE));

				mat_fragment.convertTo(mat_fragment, CV_32F);
				dct(mat_fragment, dctMat);

				Mat quanMat = dctMat / quantizationMat;
				//Mat quanMat = dctMat;
				quanMat.convertTo(quanMat, CV_8U);

				const auto zcodes = matToVecZigZag(quanMat);

				auto rle = rleEncode(zcodes);
				auto huf = Huffman();
				const auto huffcodes = huf.code(rle);

				/* ������ ������ ������� */
				builder << std::bitset<WORD>(0xff);
				builder << std::bitset<WORD>(0xc0);

				/* ���������� �������� � ������� */
				builder << std::bitset<WORD>(huf._letters.size());

				auto codes = std::stringstream();
				auto vals = std::stringstream();
				for (auto letter : huf._letters) {
					/* ������� ����� */
					builder << std::bitset<WORD>(letter->code.size());

					/* �������� �������� (1 �������� 1 ����) � ���� */
					codes << letter->code;
					vals << std::bitset<WORD>(letter->value);
				}

				builder << vals.str();
				builder << codes.str();

				/* ����� ���� � ��� ��� �������� */
				builder << std::bitset<DWORD>(huffcodes.size());
				builder << huffcodes;
			}
		}
	}
	cout << endl << "saving...   ";

	//����� ������ � ����
	const auto codeSize = builder.str().size();
	auto bytes = builder.str();
	int pointer = 0;

	std::ofstream file;
	file.open(filename, std::ios::binary | std::ios::out);
	while (true) {
		if ((pointer + WORD) > codeSize) {
			/* ���� ����� ������ ����� ��������� ������ */
			const auto offset = (pointer + WORD) - codeSize;
			const auto code   = bytes.substr(pointer, offset);

			auto byte = std::bitset<WORD>(code);
			byte = byte << offset;

			file.put(static_cast<char>(byte.to_ullong()));
			break;
		}
		const auto byte = bytes.substr(pointer, WORD);
		file.put(static_cast<char>(std::bitset<WORD>(byte).to_ullong()));
		pointer += WORD;

		print_loader();
	}

	file.close();
}

/**
 * \brief ������������� �����������
 * \param filename ������� ����
 * \return �������� �����������
 */
Mat decode(const string filename) {

	std::filebuf fb;
	std::stringstream builder2 = std::stringstream();
	const auto quantizationMat = read_examples("quantable8x8.txt", SIZE, SIZE, CV_32F);
	
	/* ��������� ����� �� ����� */
	if (fb.open(filename, std::ios::in | std::ios::out | std::ios::binary))
	{
		std::istream is(&fb);
		while (is)
			builder2 << std::bitset<WORD>(is.get());
		fb.close();
	}

	auto bytes   = builder2.str();
	auto pointer = 0;

	const auto byte1 = std::bitset<WORD>(bytes.substr(pointer, WORD));
	pointer += WORD;
	const auto byte2 = std::bitset<WORD>(bytes.substr(pointer, WORD));
	pointer += WORD;

	/* ��������� ��������� */
	if ((byte1 != std::bitset<WORD>(0xff)) || (byte2 != std::bitset<WORD>(0x44))) {
		cout << "bad file format" << endl;
		return Mat();
	}

	auto dbuffer = std::bitset<DWORD>(bytes.substr(pointer, DWORD));
	const int image_rows = static_cast<int>(dbuffer.to_ulong());
	pointer += DWORD;

	dbuffer = std::bitset<DWORD>(bytes.substr(pointer, DWORD));
	const int image_cols = static_cast<int>(dbuffer.to_ulong());
	pointer += DWORD;
	auto matrixes = vector<Mat>();
	auto rleList = vector< vector<int>>();
	
	cout << endl << "decoding...   ";

	/* ������ �����*/
	while (pointer + DWORD<bytes.size()) {
		
		print_loader();
		const auto b1 = std::bitset<WORD>(bytes.substr(pointer, WORD));
		pointer += WORD;
		const auto b2 = std::bitset<WORD>(bytes.substr(pointer, WORD));//��������� �� ���� ������
		pointer += WORD;
		
		if ((b1 != std::bitset<WORD>(0xff)) || (b2 != std::bitset<WORD>(0xc0))) {
			break;
		}

		auto buffer = std::bitset<WORD>(bytes.substr(pointer, WORD));
		const auto size = static_cast<int>(buffer.to_ulong());

		std::vector<int> codesizes(size);
		std::vector<int> values(size);
		std::vector<std::string> codes(size);

		for (int n = 0; n < size; n++) {
			pointer += WORD;
			buffer = std::bitset<WORD>(bytes.substr(pointer, WORD));
			codesizes[n] = int(buffer.to_ulong());
		}

		for (int n = 0; n < size; n++) {
			pointer += WORD;
			buffer = std::bitset<WORD>(bytes.substr(pointer, WORD));
			values[n] = int(buffer.to_ulong());
		}
		pointer += WORD;

		for (int n = 0; n < size; n++) {
			const auto s = codesizes[n];
			codes[n] = bytes.substr(pointer, s);
			pointer += s;
		}

		dbuffer = std::bitset<DWORD>(bytes.substr(pointer, DWORD));
		const auto huffcodesize = int(dbuffer.to_ulong());
		pointer += DWORD;

		const auto codeStr = bytes.substr(pointer, huffcodesize);
		pointer += huffcodesize;

		auto h = Huffman();
		auto letters = std::vector<Huffman::Letter*>();
		for (int i = 0; i < size; i++) {
			auto l = new Huffman::Letter();
			l->code = codes[i];
			l->value = values[i];
			letters.push_back(l);
			h._codemap[l->value] = l->code;
		}

		h._letters = letters;
		auto rle = h.decode(codeStr);
		rleList.push_back(rle);
		auto res = vector<int>();
		rleDecode(rle, res);
		auto matrix = vecToMatZigZag(res);
		matrix.convertTo(matrix, CV_32F);

		Mat quanMat = matrix.mul(quantizationMat);
		//Mat quanMat = matrix;
		idct(quanMat, quanMat);

		quanMat.convertTo(quanMat, CV_8U);
		matrixes.push_back(quanMat);
	}

	int count = 0;
	Mat resultImg = Mat(image_rows, image_cols, CV_8U, Scalar(0, 0, 0));
	for (int i = 0; i < resultImg.cols; ++i) {
		for (int j = 0; j < resultImg.rows; ++j) {
			auto modI = i % SIZE;
			auto modJ = j % SIZE;
			if (!modI && !modJ) {
				const auto tmpmat = matrixes[count];
				tmpmat.copyTo(resultImg(Rect(i, j, tmpmat.cols, tmpmat.rows)));
				count++;
			}
		}
	}
	return  resultImg;
}

/**
* \brief ��������� ������ �� ��������� ���������� SIZE
* \param size ������� ������ �������
* \return ����������� ������
*/
int calc_mat_size(const int size) {
	const int tmp = size % SIZE;
	return size + ((tmp) ? SIZE - tmp : 0);
}

/**
 * \brief ���������� ������ �� ����� � ������
 * \param filename	 ��� �����
 * \param rows	     ������ �������
 * \param cols	     ������ �������
 * \param type       ��� ������� (�� ��������� CV_32F)
 * \return ���������� �������
 */
auto read_examples(const string filename, const int rows, const int cols, const int type = CV_32F)-> Mat {
	string s;
	Mat data = Mat(rows, cols, type);
	
	std::ifstream file(filename);
	if (file.is_open()) {
		int i = 0;
		while (getline(file, s)) {
			std::vector<std::string> lines = Huffman::split(s, ' ');
			const int lenght = int(lines.size());
			
			for (int j = 0; j < lenght; j++) {
				float num;
				const auto str = lines[j];
				std::istringstream(str) >> num;
				data.at<float>(i, j) = num;
			}
			i++;
			if (i >= rows) break;
		}
	}
	file.close();
	return data;
}

/**
 * \brief �������� ������ ������������
 * \param callback �������, ������� ���������� ��� ��������� ������� ��������
 */
void getZigzagCode(const std::function<void(int, int, int)> callback) {
	const int lastValue = SIZE * SIZE - 1;
	int currNum  = 0;
	int currDiag = 0;
	int loopFrom, loopTo, row, col;
	do {
		if (currDiag < SIZE) {
			loopFrom = 0;
			loopTo   = currDiag;
		}
		else {
			loopFrom = currDiag - SIZE + 1;
			loopTo   = SIZE - 1;
		}
		for (int i = loopFrom; i <= loopTo; i++) {
			if (currDiag % 2 == 0) {
				row = loopTo - i + loopFrom;
				col = i;
			}
			else {
				row = i;
				col = loopTo - i + loopFrom;
			}
			callback(row, col, currNum);
			currNum++;
		}
		currDiag++;
	} while (currDiag <= lastValue);
}

/**
* \brief c�������� �������� ������� ��������
* \param matrix �������
* \return ������ ��������
*/
vector<int> matToVecZigZag(Mat& matrix) {
	std::vector<int> codes = std::vector<int>();
	getZigzagCode([matrix, &codes](int row, int col, int currNum) -> void {
		codes.push_back(int(matrix.at<uchar>(row, col)));
	});		
	return codes;
}

/**
* \brief ���������� �������� ������� � ������� ��������
* \param codes ������ ��������
* \return �������
*/
Mat vecToMatZigZag(std::vector<int>& codes) {
	Mat mat = cv::Mat(SIZE, SIZE, CV_32SC1);
	getZigzagCode([&mat, codes](int row, int col, int currNum) -> void {
		mat.at<int>(row, col) = codes[currNum];
	});
	return mat;
}


/**
 * \brief ����������� ���
 * \param vec �������� ������
 * \return �������������� ������
 */
vector<int>  rleEncode(std::vector<int> vec) {
	std::vector<int> codes = std::vector<int>();
	int nulCount = 0;

	for (int i = 0; i < vec.size(); ++i) {
		auto c = vec[i];
			if (c == 0 && (i != vec.size()-1)) {
				nulCount++;
			}
			else {
				codes.push_back(nulCount);
				codes.push_back(c);
				if (nulCount) {
					nulCount = 0;
				}
			}
	}
	return codes;
}


/**
 * \brief ������������� ���
 * \param codes �������������� ������
 * \param result ��������������� ������
 */
void rleDecode(std::vector<int>& codes, std::vector<int>& result) {
	result.clear();
	const auto size = codes.size();
	for(auto i = 0; i < size; i += 2) {
		const auto count = codes[i];
		auto value = codes[i+1];
		int n = 0;
		while (n < count) {
			result.push_back(0);
			n++;
		}
		result.push_back(value);
	}
}


/**
* \brief ����� <uchar> ������� � �������
* \param mat �������
* \param str ���������
*/
void printMat(Mat mat, string str = "mat") {
	cout << endl << "===" << str << "===" << endl;
	for (int i = 0; i < mat.cols; ++i) {
		for (int j = 0; j < mat.rows; ++j) {
			cout << int(mat.at<uchar>(j, i)) << " ";
		}
		cout << endl;
	}
}

/**
 * \brief ����� ������� � �������
 * \tparam T ��� �������� ���������� �� �������
 * \param mat �������
 * \param str ���������
 */
template<typename T>
void printMat(Mat mat, string str = "mat") {
	cout << endl << "===" << str << "===" << endl;
	for (int i = 0; i < mat.cols; ++i) {
		for (int j = 0; j < mat.rows; ++j) {
			cout << mat.at<T>(j, i)<< " \t";
		}
		cout << endl;
	}
}


/**
* \brief ������� �������� �������� � �������
*/
void print_loader() {
	static auto counter = 0;

	const auto index = counter % 4;
	string s = "";

	switch (index) {
	case 0:
		s = "-";
		break;
	case 1:
		s = "\\";
		break;
	case 2:
		s = "|";
		break;
	default:
		s = "/";
		break;
	}

	counter++;
	cout << "\b" << s << std::flush;
}
