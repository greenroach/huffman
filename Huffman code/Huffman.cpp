#include "stdafx.h"
#include "Huffman.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
using std::cout;
using std::endl;

//������ ����� �� �����������
template<typename Out>
auto Huffman::split(const std::string &s, char sep, Out result)-> void {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, sep)) {
		*(result++) = item;
	}
}

auto Huffman::split(const std::string &s, char sep)-> std::vector<std::string> {
	std::vector<std::string> elems;
	split(s, sep, std::back_inserter(elems));
	return elems;
}

Huffman::Letter::Letter():value(0) {}

Huffman::Letter::~Letter() {}

Huffman::Huffman() {}

Huffman::~Huffman() {}

void Huffman::build(std::vector<int> img) {
	_letters.clear();
	_codemap.clear();
	Node::lastNodes.clear();

	/*������� ����������� ��������*/
	std::map<unsigned int, unsigned int> histo;
	const auto count = img.size();
	for (auto pixel : img) {
		const auto c = histo.count(pixel);
		if (c == 0) {
			histo[pixel] = 1;
		}
		else {
			histo[pixel] += 1;
		}
	}

	/*���������� ������ ������������*/
	std::vector<Node*> nodes;
	for (auto val : histo) {
		auto node = new Node();
		node->value = val.first;
		node->p = float(val.second) / count;
		nodes.push_back(node);
	}

	/*��������� �� ����������� �����������*/
	sortNodes(nodes);

	/*������ �������� ������*/
	/* ���� � ������ �� ��������� ������ ���� ��� */
	while (nodes.size() > 1) {

		/* ����� ������ 2 ����������� (����� ���������) */
		auto node0 = nodes[0];
		auto node1 = nodes[1];
		/* ���������� � ����� ��� */
		auto newNode = new Node(node0->p + node1->p);
		/* ������� � ����� ���� */
		node0->linkTo(newNode);
		node1->linkTo(newNode);
		/* ������� ����� ���� �� ������� */
		newNode->linkTo(node0, node1);

		/* ������� 2 ������ ���� �� ������� */
		nodes.erase(nodes.begin(), nodes.begin() + 2);
		/* ��������� � ������ ����� ������������ ���� */
		nodes.push_back(newNode);

		/*��������� ������ �� ����� ���� (��� ��� ���� ����� �����, �� ��������� ��� ����� ������ ������) */
		Node::root = newNode;

		//��������� ������
		sortNodes(nodes);
	}

	/*������������� ���� ��� ����� ������� � �����*/
	setCode(Node::root);

	//����������� ����� ����� 
	for (auto last_node : Node::lastNodes) {
		auto l = new Letter(); //������� ������
		l->value = last_node->value; //���������� �������� �������
		auto str = readCode(last_node); //���������� ��������� ��� (����� �������)
		auto rts = std::string(str.rbegin(), str.rend()); //�������������

		l->code = rts; //���������� ��� ��� �������� �������
		_codemap[l->value] = l->code;
		_letters.push_back(l); //��������� � ������
	}
}

vector<int> Huffman::decode(std::string code) {
	std::vector<int> values;
	
	std::string buf = "";

	auto result = vector<int>();
	//������ ������� ���� � �������� � ������������� ������� �����
	std::map<std::string, uchar> swappedCodeMap;
	for (std::map<uchar, std::string>::iterator it = _codemap.begin(); it != _codemap.end(); ++it)
		swappedCodeMap[it->second] = it->first;
		
		//������ �� ������ ������� ���� �� ������ ���������� �� ����, ����� ���� ���������� ������� ������ ������� � ��������
		for (char symbol : code) {
			buf += symbol;
			if (swappedCodeMap.count(buf)) {
				result.push_back(int(swappedCodeMap[buf]));
				buf.clear();
			}
		}
	return result;
}

std::string Huffman::code(std::vector<int>& img) {
	
	build(img);
	std::stringstream builder = std::stringstream();

	for (auto pixel : img) {
		builder << _codemap[pixel];
	}
	return builder.str();

}

void Huffman::sortNodes(std::vector<Node *> &nodes) {
	std::sort(nodes.begin(), nodes.end(), [=](Node* a, Node *b)	{
		return a->p < b->p;
	});
}

std::string Huffman::readCode(Node * node) {
	const auto prev = node->prev;
	if (prev == nullptr) {
		return "";
	}
	std::string s = (node->code)?"1":"0";
	return s += readCode(prev);
}

void Huffman::setCode(Node * node) {
	const auto next0 = node->next0;
	const auto next1 = node->next1;
	if ((next0 == nullptr) && (next1 == nullptr)) {
		Node::lastNodes.push_back(node);
		return;
	}
	next0->code = 0;
	next1->code = 1;
	setCode(next0);
	setCode(next1);
}