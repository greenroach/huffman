#pragma once
#include <vector>

class Node {
public:
	Node* prev;
	Node* next0;
	Node* next1;
	
	float p;
	unsigned char code;
	unsigned char value;
	
	Node();
	Node(float p);
	~Node();
	
	void Node::linkTo(Node*prev, Node*next0, Node*next1);
	void Node::linkTo(Node*next0, Node*next1);
	void Node::linkTo(Node*prev);

	static Node* root; //������ �� ������ ������
	static std::vector<Node *> lastNodes; //������ ������������ ��������� ������
};

