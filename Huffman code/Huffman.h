#pragma once
#include <map>
#include <vector>
#include "Node.h"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;

class Huffman
{
public:
	/*�������� ��� �������� �������*/
	class Letter {
	public:
		/**
		* \brief �������� �������
		*/
		unsigned char value;
		/**
		* \brief ���
		*/
		std::string code;
		Letter();
		~Letter();
	};
	Huffman();
	~Huffman();

	/**
	 * \brief ������ ������ ����� ��������
	 * \param img �������� ��� ������� �������� ������
	 */
	void build(std::vector<int> img);

	/**
	 * \brief ���������� ������������� ��������
	 * \param image ���� �� ��������
	 * \return �������������� �����������
	 */
	vector<int> decode(std::string image);

	/**
	 * \brief �������� �����������
	 */
	std::string code(std::vector<int>&);

	/**
	 * \brief ������������� ���� ��� �����
	 * \param node ������� ���
	 */
	static void setCode(Node * node);

	/**
	 * \brief ��������� ���� �����
	 * \param node ������� ���
	 * \return 
	 */
	static std::string readCode(Node * node);
	static std::vector<std::string> split(const std::string &s, char sep);
	std::map<uchar, std::string> _codemap;
	/**
	* \brief ������ ��������(��� � ��������)
	*/
	std::vector<Letter*> _letters;
protected:

	/**
	 * \brief ��������� ���� ������
	 * \param nodes ������ �����
	 */
	void sortNodes(std::vector<Node *> &nodes);
	

	/*����� ������ �� �����������*/
	template<typename Out>
	static void split(const std::string &s, char sep, Out result);
};

